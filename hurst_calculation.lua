local hurst_calculation = {}

hurst_calculation.linreg = function(x, y)
	local a, b = 0, 0
	local xs, x2s, ys, xys = 0, 0, 0, 0
	
	for ix = 1, #x, 1 do
		if y[ix] ~= nil then
			xs = xs + x[ix]
			ys = ys + y[ix]
			x2s = x2s + math.pow(x[ix], 2)
			xys = xys + (x[ix] * y[ix])
		end
	end
	
	b = (#x*xys - xs * ys)/(#x*x2s - math.pow(xs, 2))
	a = (ys - b * xs)/#x
	
	return a, b
end

hurst_calculation.ln_array = function(arr)
	local retArr = {}
	
	for ix, iy in pairs(arr) do
		if iy ~= 0 then
			table.insert(retArr, math.log(iy))
		end
	end
	
	return retArr
end

hurst_calculation.array_to_string = function(arr)
	local retStr = ""
	
	for ix, iy in pairs(arr) do
		retStr = string.format("%s[%d]:%f", retStr, ix, iy)
		if ix < #arr then
			retStr = string.format("%s, ", retStr)
		end
	end
	
	return retStr
end 

hurst_calculation.matrix_to_array = function(matrix)
	local ret_arr = {}
	
	for ix = 1, #matrix, 1 do
		for iy = 1, #matrix[ix], 1 do 
			table.insert(ret_arr, matrix[ix][iy])
		end
	end
	
	return ret_arr
end

hurst_calculation.subarray = function(arr, a, b)
	local ret_arr = {}
	local n_arr = {}
	
	for ix = a, b, 1 do
		table.insert(ret_arr, arr[ix])
		table.insert(n_arr, ix)
	end
	
	return ret_arr, n_arr
end

hurst_calculation.calculate_mean = function(arr)
	local ret_mean = 0
	
	for ix = 1, #arr, 1 do 
		if arr[ix] == arr[ix] then
			ret_mean = ret_mean + arr[ix]
		end
	end
	ret_mean = ret_mean/#arr
	
	return ret_mean
end

hurst_calculation.calculate_dev = function(arr, mean)
	local arr_dev = {}
	
	for ix = 1, #arr, 1 do 
		table.insert(arr_dev, arr[ix]-mean)
	end
	
	return arr_dev
end

hurst_calculation.calculate_cumdev = function(arr)
	local arr_cumdev = {}
	
	local buf = 0
	for ix = 1, #arr, 1 do 
		buf = buf + arr[ix]
		table.insert(arr_cumdev, buf)
	end
	
	return arr_cumdev
end

hurst_calculation.calculate_stdev = function(arr, mean)
	local ret_stdev = 0
	
	for ix = 1, #arr, 1 do 
		ret_stdev = ret_stdev + math.pow(arr[ix]-mean, 2)
	end
	
	ret_stdev = math.sqrt(ret_stdev/#arr)
	
	return ret_stdev
end

hurst_calculation.calculate_R_value = function(cumdev_arr)
	local ret_R = 0
	
	local max_cd, min_cd = 0, 10000000
	for ix = 1, #cumdev_arr, 1 do
		if cumdev_arr[ix] > max_cd then 
			max_cd = cumdev_arr[ix]
		end
		if cumdev_arr[ix] < min_cd then
			min_cd = cumdev_arr[ix]
		end
	end
	
	ret_R = max_cd - min_cd
	
	return ret_R
end

hurst_calculation.calculate_lo_S = function(arr, p_val, mean)
	local ret_lo_S = 0
	
	for ip = 1, p_val, 1 do
		local buf_omega = 1 - ip / (p_val - 1)
		
		local buf_arr_sum = 0
		for ix = ip + 1, #arr, 1 do
			buf_arr_sum = buf_arr_sum + (arr[ix] - mean) * (arr[ix - 1] - mean)
		end
		ret_lo_S = ret_lo_S + buf_omega * buf_arr_sum
	end
	ret_lo_S = (2 * ret_lo_S) / #arr
	
	return ret_lo_S
end

hurst_calculation.get_periods_len = function(arr_len, p_start, div_value)
	local period_start, period_end, ret_arr_len = p_start, 0, 0
	
	if math.fmod(math.floor(arr_len), div_value) == 0 then
		period_end = math.floor(arr_len)/div_value
	else
		period_end = math.floor(arr_len-1)/div_value
	end
	ret_arr_len = period_end*div_value
	
	return period_start, period_end, ret_arr_len
end

hurst_calculation.RS_method = function(arr)
	local period_start, period_end, arr_size = hurst_calculation.get_periods_len(#arr, 2, 2)
	
	local RS_arr = {}
	local periods_arr = {}
	
	for ix = period_start, period_end, 1 do	
		local buf_RS_arr = {}
		for iy = 1, math.floor(arr_size/ix), 1 do
			local buf_arr = hurst_calculation.subarray(arr, ix*(iy-1)+1, ix*iy)
			local buf_mean = hurst_calculation.calculate_mean(buf_arr)
			local buf_dev = hurst_calculation.calculate_dev(buf_arr, buf_mean)
			local buf_cumdev = hurst_calculation.calculate_cumdev(buf_dev)
			local buf_R = hurst_calculation.calculate_R_value(buf_cumdev)
			local buf_S = hurst_calculation.calculate_stdev(buf_arr, buf_mean)
			local buf_RS = buf_R/buf_S
			
			table.insert(buf_RS_arr, buf_RS)
		end
		
		local RSt = hurst_calculation.calculate_mean(buf_RS_arr)
		
		table.insert(periods_arr, ix)
		table.insert(RS_arr, RSt)
	end
	
	return periods_arr, RS_arr
end

hurst_calculation.MFDFA_method = function(arr)
	local period_start, period_end, arr_size = hurst_calculation.get_periods_len(#arr, 2, 4)

	local Fq_arr = {}
	local m_arr = {}
	
	for im = period_start, period_end, 1 do
		local cumdev_arr, buf_sum = {}, 0
		local arr_mean = hurst_calculation.calculate_mean(arr)
		
		for it = 1, arr_size, 1 do
			buf_sum = buf_sum + (arr[it] - arr_mean)
			table.insert(cumdev_arr, buf_sum)
		end
		
		local subarr_quantity =  math.floor(arr_size / im)
		
		local f2st_array = {}
		for ix = 1, subarr_quantity, 1 do
			local buf_arr, buf_n_arr = hurst_calculation.subarray(cumdev_arr, (1 + ix * im), im * (ix + 1))
			for iy = #buf_arr, 1, -1 do
				table.insert(buf_arr, buf_arr[iy])
				table.insert(buf_n_arr, buf_n_arr[iy])
			end
			
			local buf_a, buf_b = hurst_calculation.linreg(buf_n_arr, buf_arr)	
			
			local buf_detrended_arr = {}
			for iy = 1, #buf_arr, 1 do
				table.insert(buf_detrended_arr, buf_arr[iy] * (1 - buf_a) - buf_b )
			end

			local buf_z = 0
			for iy = 1, #buf_detrended_arr, 1 do
				buf_z = buf_z + (buf_detrended_arr[iy])^2
			end
			buf_z = buf_z / #buf_detrended_arr
			
			table.insert(f2st_array, buf_z)
		end
		
		local fq_sum = 0
		for ix = 1, #f2st_array, 1 do
			fq_sum = fq_sum + f2st_array[ix]
		end
		fq_sum = math.sqrt(fq_sum / #f2st_array)
		if fq_sum == fq_sum then
			table.insert(Fq_arr, fq_sum)
			table.insert(m_arr, im)
		end
	end
	
	return m_arr, Fq_arr
end

hurst_calculation.periodogram_method = function(arr)
	local period_start, period_end, arr_size = hurst_calculation.get_periods_len(#arr, 2, 2)

	local Ul_arr = {}
	local l_arr = {}

	for il = period_start, period_end, 1 do
		local arr_sum = 0
		
		for it = 1, arr_size, 1 do
			arr_sum = arr_sum + arr[it] * (-1)^math.floor((-2 * it * il) / arr_size)
		end
		arr_sum = (arr_sum^2) / (2 * math.pi * arr_size)
		
		table.insert(Ul_arr, arr_sum)
		table.insert(l_arr, (2 * math.pi * il) / arr_size)
	end
	
	return l_arr, Ul_arr
end

hurst_calculation.local_whittle_method = function(l_arr, arr)
	local Rh_min = 1000000
	local Rh_arr, H_arr = {}, {}
	local ret_H = 0
	
	for id = -0.5, 0.5, 0.0001 do
		local Rh, Rsum = 0, 0
		
		for im = 1, #arr, 1 do
			Rh = Rh + arr[im] * (l_arr[im])^(2 * id)
		end
		Rh = math.log(Rh / #arr)
		
		for im = 1, #arr, 1 do
			Rsum = Rsum + math.log(l_arr[im])
		end
		Rsum = (2 * id / #arr) * Rsum
		
		Rh = Rh - Rsum
		
		if Rh < Rh_min then
			ret_H = 0.5 + id
			Rh_min = Rh
		end
		table.insert(Rh_arr, Rh)
		table.insert(H_arr, 0.5 + id)
	end
	
	return ret_H, Rh_arr, H_arr
end

return hurst_calculation
