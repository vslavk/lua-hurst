---

## lua-hurst (eng.)

This repository includes a program for calculating the Hurst coefficient based on traffic received using the Wireshark. The following methods of calculating Hurst were used:

1. RS analysis.
2. MFDFA analysis.
3. Periodogram method.
4. Whittle's Method.

The application was tested under the Debian 9 Linux distribution and the luaJIT interpreter was used. There are no external dependencies.  

The following sources were used to develop this application:

1. Zhang, D., Liu, Y., Anani, A., Li, H. Improved R/S Algorithm Based on Network Traffic Self-Similarity. 2008 4th International Conference on Wireless Communications, Networking and Mobile Computing. DOI:10.1109/wicom.2008.2936 
2. Jan W. Kantelhardt, Stephan A. Zschiegner, Eva Koscielny-Bunde, Shlomo Havlin, Armin Bunde, H. Eugene Stanley. Multifractal detrended fluctuation analysis of nonstationary time series. Physica A 316 (2002) 87 – 114. Elsevier Science B.V. 2002. P. 87-114. DOI: 10.1016/S0378-4371(02)01383-3. 
3. G. Millán, E. S. Juan and M. Jamett. A Simple Estimator of the Hurst Exponent for Self-Similar Traffic Flows. IEEE LATIN AMERICA TRANSACTIONS, VOL. 12, NO. 8, DECEMBER 2014. P. 1349-1354.  DOI: 10.1109/TLA.2014.7014500.
4. ALBERTO CONTRERAS-CRISTÁN, EDUARDO GUTIÉRREZ-PEÑA, STEPHEN G. WALKER. Note on Whittle’s Likelihood. Communications in Statistics—Simulation and Computation ® , 35: 857–875, 2006. DOI: 10.1080/03610910600880203.

---

## lua-hurst (рус.)

Данный репозиторий включает в себя программу для расчета коэффициента Хёрста по трафику, полученному с помощью утилиты Wireshark. Были использованы следующие методы расчета Хёрста:

1. RS-анализ.
2. MFDFA-анализ.
3. Метод периодограмм.
4. Метод Виттла.

Работоспособность приложения проверялось под дистрибутивом Linux Debian 9, использовался интерпретатор luaJIT. Внешние зависимости отсутствуют.  

Для разработки приложения использовались следующие источники:

1. Zhang, D., Liu, Y., Anani, A., Li, H. Improved R/S Algorithm Based on Network Traffic Self-Similarity. 2008 4th International Conference on Wireless Communications, Networking and Mobile Computing. DOI:10.1109/wicom.2008.2936 
2. Jan W. Kantelhardt, Stephan A. Zschiegner, Eva Koscielny-Bunde, Shlomo Havlin, Armin Bunde, H. Eugene Stanley. Multifractal detrended fluctuation analysis of nonstationary time series. Physica A 316 (2002) 87 – 114. Elsevier Science B.V. 2002. P. 87-114. DOI: 10.1016/S0378-4371(02)01383-3. 
3. G. Millán, E. S. Juan and M. Jamett. A Simple Estimator of the Hurst Exponent for Self-Similar Traffic Flows. IEEE LATIN AMERICA TRANSACTIONS, VOL. 12, NO. 8, DECEMBER 2014. P. 1349-1354.  DOI: 10.1109/TLA.2014.7014500.
4. ALBERTO CONTRERAS-CRISTÁN, EDUARDO GUTIÉRREZ-PEÑA, STEPHEN G. WALKER. Note on Whittle’s Likelihood. Communications in Statistics—Simulation and Computation ® , 35: 857–875, 2006. DOI: 10.1080/03610910600880203.

---
