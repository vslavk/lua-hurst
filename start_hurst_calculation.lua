-------------------Include-Hurst-calculation-file-----------------------
local hcalc = require("hurst_calculation")

---------------------Functions-to-read-csv-file-------------------------
function lines_from(filename)
	local lines = {}
	local file = io.open(filename, "rb")
	if file == nil then 
		io.close(file)
		return false 
	end
	
	for line in io.lines(filename) do 
		lines[#lines + 1] = line
	end
	
	io.close(file)
	
	return lines
end

function split(s, delimiter)
    local result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

------------------Get-csv-file-name-as-argument-------------------------
if #arg < 2 then 
	print("Error! Write wireshark's csv file name as argument and 1 (if yes) or 0 (if not) to write hurst trends to file")
	os.exit(0)
end

local filename = arg[1]
---------------------Read-and-parse-csv-file----------------------------
local lines = lines_from(filename)

for ix = 2, table.getn(lines) do
	lines[ix-1] = lines[ix]
end
table.remove(lines, table.getn(lines))

local csv_obj = {}
local time_arr = {}

local interval_arr = {}

for ix,iy in pairs(lines) do
	local buf = split(iy, ",")
	--local buf = split(iy, ";")
	buf[1] = buf[1]:gsub('"', '')
	buf[2] = buf[2]:gsub('"', '')
	--buf[2] = buf[2]:gsub(',', '.')
	--buf[6] = buf[6]:gsub('"', '')
	table.insert(csv_obj, buf)
	table.insert(time_arr, buf[2])
end

----------------Get-package-rate-time-intervals-------------------------
for ix = 1, #time_arr, 1 do
	local buf = tonumber(time_arr[ix]) - tonumber(time_arr[ix-1])
	--local buf = time_arr[ix]
	table.insert(interval_arr, buf) 
end 

------------------------------------------------------------------------
---------------------------RS-method------------------------------------
------------------------------------------------------------------------
local index_arr, RS_arr = hcalc.RS_method(interval_arr)
local ln_indexes, ln_RSs = hcalc.ln_array(index_arr), hcalc.ln_array(RS_arr)
local trend_a, trend_b = hcalc.linreg(ln_indexes, ln_RSs)
local H_RS = trend_b
if tonumber(arg[2]) == 1 then
	local file = io.open("rs_data.csv", "w+")
	
	for ix = 1, #ln_indexes, 1 do
		file:write(string.format("%f, %f, %f\n", ln_indexes[ix], ln_RSs[ix], trend_a * ln_indexes[ix] + trend_b))
	end
	
	file:close()
end
print("Hurst by RS analysis method is: " .. H_RS)
------------------------------------------------------------------------
---------Modified-Multifractal-Detrended-Fluctuation-Analysis-----------
------------------------------------------------------------------------
local index_arr, MFDFA_arr = hcalc.MFDFA_method(interval_arr)
local ln_indexes, ln_MFDFAs = hcalc.ln_array(index_arr), hcalc.ln_array(MFDFA_arr)
trend_a, trend_b = hcalc.linreg(ln_indexes, ln_MFDFAs)	
local H_MFDFA = trend_b + 1
if tonumber(arg[2]) == 1 then
	local file = io.open("mfdfa_data.csv", "w+")
	
	for ix = 1, #ln_indexes, 1 do
		file:write(string.format("%f, %f, %f\n", ln_indexes[ix], ln_MFDFAs[ix], trend_a * ln_indexes[ix] + trend_b))
	end
	
	file:close()
end
print("Hurst by MFDFA method is: " .. H_MFDFA)
------------------------------------------------------------------------
-----------------------Periodogram-method-------------------------------
------------------------------------------------------------------------
local index_arr, periodogram_arr = hcalc.periodogram_method(interval_arr)
local ln_indexes, ln_periodograms = hcalc.ln_array(index_arr), hcalc.ln_array(periodogram_arr)
trend_a, trend_b = hcalc.linreg(ln_indexes, ln_periodograms)	
local H_Periodogram = 0.5 - trend_b/2
if tonumber(arg[2]) == 1 then
	local file = io.open("periodogram_data.csv", "w+")
	
	for ix = 1, #ln_indexes, 1 do
		file:write(string.format("%f, %f, %f\n", ln_indexes[ix], ln_periodograms[ix], trend_a * ln_indexes[ix] + trend_b))
	end
	
	file:close()
end
print("Hurst by Periodogram method is: ".. H_Periodogram)
------------------------------------------------------------------------
------------------------Local-Whittle-method----------------------------
------------------------------------------------------------------------
local H_LWM, Rh_arr, H_arr = hcalc.local_whittle_method(index_arr, periodogram_arr)
if tonumber(arg[2]) == 1 then
	local file = io.open("whittle_data.csv", "w+")
	
	trend_a, trend_b = hcalc.linreg(H_arr, Rh_arr)
	for ix = 1, #Rh_arr, 1 do
		file:write(string.format("%f, %f, %f\n", H_arr[ix], Rh_arr[ix], trend_a * H_arr[ix] + trend_b))
	end
	
	file:close()
end
print("Hurst by local Whittle method is: ".. H_LWM)
